#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
// g++ -m32 -Wall -o 1.out 1.cpp -L"C:\Program Files (x86)\Dev-Cpp\MinGW32\bin" -lglu32 -lglut32 -lopengl32 -lstdc++

GLfloat x = 10.0;
GLfloat y = 10.0;
GLfloat z = 10.0;

GLfloat x1 = 1.0;
GLfloat y2 = 1.0;
GLfloat z3 = 1.0;

void display () {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();

    glTranslatef(0,5,0);
    glPushMatrix();                   // roof
    glutSolidCube(1);
    glPopMatrix();

    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_LINES);
    for (GLfloat i = -2.5; i <= 2.5; i += 0.25) {
      glVertex3f(i, 0, 2.5);
      glVertex3f(i, 0, -2.5);
      glVertex3f(2.5, 0, i);
      glVertex3f(-2.5, 0, i);
    }
    glEnd();

    glTranslatef(0,-3,0);
    glPushMatrix();                   // roof
    glRotatef(-90,1,0,0);
    glutSolidCone(1.5,1,16,8);
    glPopMatrix();

    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_LINES);
    for (GLfloat i = -2.5; i <= 2.5; i += 0.25) {
      glVertex3f(i, 0, 2.5);
      glVertex3f(i, 0, -2.5);
      glVertex3f(2.5, 0, i);
      glVertex3f(-2.5, 0, i);
    }
    glEnd();

    glTranslatef(0,-5,0);
    glPushMatrix();
    glRotatef(-90,1,0,0);
    glutSolidTorus(1,1,16,8);
    glPopMatrix();

    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_LINES);
    for (GLfloat i = -2.5; i <= 2.5; i += 0.25) {
      glVertex3f(i, 0, 2.5);
      glVertex3f(i, 0, -2.5);
      glVertex3f(2.5, 0, i);
      glVertex3f(-2.5, 0, i);
    }
    glEnd();
    glPopMatrix();
    glFlush();

}


void OnKeyPress(unsigned char key, int, int)
{
    std::cout << key << std::endl;
    // switch (key)
    // {
    // // Aumentar el LOD
    // case '+':
    //     ++LOD;
    //     break;
    // // disminuye el LOD
    // case '-':
    //     --LOD;
    //     // tiene un valor mínimo de LOD
    //     if (LOD < 3)
    //         LOD = 3;
    //     break;
    // default:
    //     break;
    // }
    // // Pide a Glut que vuelva a dibujar la pantalla para nosotros ...
    // glutSwapBuffers();
    // glutPostRedisplay();
}
void specialKeyInput(int key, int x, int y){

    // std::cout<<key<<" "<<GLUT_KEY_LEFT<<" "<<GLUT_KEY_RIGHT<<"\n";
    

    if(key == GLUT_KEY_LEFT){
// x++;
//       y++;
      z+=0.01;
        gluLookAt(10.0,10.0,10.0,0.0,0.0,0.0,0.0,0.0,z);
    }
      
    // if(key == GLUT_KEY_RIGHT)
    //   angle -= 5.0;
// cout<<angle<<"\n";
    // gluLookAt(5.0,5.0,5.0,0.0,0.0,0.0,0.0,0.1,.0);
    glutPostRedisplay();

}
void reshape ( int width, int height ) {

    /* define the viewport transformation */
    glViewport(0,0,width,height);
}

int main ( int argc, char * argv[] ) {

    /* initialize GLUT, using any commandline parameters passed to the 
       program */
    glutInit(&argc,argv);

    /* setup the size, position, and display mode for new windows */
    glutInitWindowSize(500,500);
    glutInitWindowPosition(0,0);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH);

    /* create and set up a window */
    glutCreateWindow("hello, teapot!");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glutKeyboardFunc(OnKeyPress);
    glutSpecialFunc(specialKeyInput);
    /* set up depth-buffering */
    glEnable(GL_DEPTH_TEST);

    /* turn on default lighting */
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    /* define the projection transformation */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(40,1,4,20);

    /* define the viewing transformation */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // gluLookAt(5.0,5.0,5.0,0.0,0.0,0.0,0.0,0.0,1.0);
    gluLookAt(x,y,z,0.0,0.0,0.0,0.0,1.0,1.0);

    /* tell GLUT to wait for events */
    glutMainLoop();
}